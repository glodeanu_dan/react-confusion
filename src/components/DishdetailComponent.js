import React from 'react'
import { Card, CardImg, CardImgOverlay, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap'
import { Link } from 'react-router-dom'
import CommentForm from './CommentForm'
import { Loading } from './LoadingComponent'
import { baseUrl } from '../shared/baseUrl'
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

function RenderDish({dish}) {
    return (
        <FadeTransform in 
            transfromProps={{
                exitTransform: 'scale(0.5 translateY(-50%)'
            }}>
            <Card>
                <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name}/>
                <CardBody>
                    <CardTitle>{dish.name}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </CardBody>
            </Card>
        </FadeTransform>
    )
}

function RenderComments({comm, postComment, dishId}) {
        return (
            <div>
                <h4 className="text-left">Comments</h4>
                <ul className="list-unstyled">
                    <Stagger in>
                        {comm.map((elem) => {
                            return (
                                <Fade in>
                                    <li key={elem.date} className="text-left">
                                        {elem.comment}
                                        <br></br>
                                        Rating: {elem.rating}
                                        <br></br>
                                        -- {elem.author}, {new Intl.DateTimeFormat("en-US", {
                                        year: "numeric",
                                        month: "long",
                                        day: "2-digit"
                                        }).format(new Date(elem.date))}
                                        <br></br>
                                        <br></br>
                                    </li>
                                </Fade>
                            )
                        })}
                    </Stagger>
                    <CommentForm dishId={dishId} postComment={postComment} />
                </ul>
            </div>
        )
    }

const DishDetail = (props) => {
    if (props.isLoading) {
        return (
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        );
    }
    else if (props.errMess) {
        return (
            <div className="container">
                <div className="row">
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    }
    else if (props.dish != null)  {
        return (    
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                        <hr/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-md-5 m-1">
                        <RenderDish dish={props.dish} />
                    </div>
                    <div className="col-12 col-md-5 m-1">
                        <RenderComments comm={props.comments}
                        postComment={props.postComment}
                        dishId={props.dish.id} />
                    </div>                
                </div>
            </div>
        );
    }
    return (
        <div></div>
    );
}


export default DishDetail;